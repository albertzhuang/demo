#include <cstdio>
#include <cmath>
#include <random>
#include <ctime>
#include <vector>
#include <algorithm>
#include "softmax.h"

#ifndef VERSION
    #define VERSION dev
#endif
#define TO_STR(x) TO_STR_(x)
#define TO_STR_(x) #x


const char * get_version() {
    return TO_STR(VERSION);
}


int main(){

    printf("Demo Softmax (version %s)\ninput a number n:\n", get_version());

    size_t N;
    scanf("%zu", &N);

    std::vector<float> input(N);
    std::vector<float> output(N);

    srand(time(NULL));
    std::generate(
        std::begin(input),
        std::end(input),
        rand
    );

    softmax(&input[0], N, &output[0] );

    for( size_t i = 0; i < N; i++){
        printf("input[%zu] = %7.4f; output[%zu] = %7.4f\n", i, input[i], i, output[i]);
    }

    return 0;
}