#pragma once
#include <cstdlib>

void distance(
    float* input1, 
    float* input2, 
    size_t N, 
    float* output
);
