#ifndef SOFTMAX_H
#define SOFTMAX_H


void softmax(float *input, size_t len, float *output);

#endif