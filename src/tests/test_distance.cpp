#include "catch.hpp"
#include "distance.h"

TEST_CASE("distance") {
    float input1[] = {0, 0};
    float input2[] = {1, 1};
    float output;
    float answer = 1.414;

    distance(input1, input2, 2, &output);

    REQUIRE( output == Approx(answer).margin(1e-3) );
}

TEST_CASE("distance - large") {
    float n = std::numeric_limits<float>::max() / 2;

    float input1[] = {0, 0};
    float input2[] = {n, n};
    float output;
    float answer = sqrt(2) * n;

    distance(input1, input2, 2, &output);

    REQUIRE( output == Approx(answer).margin(1e-4) );
}