#include <cmath>

#include "catch.hpp"
#include "softmax.h"
#include <cstdio>

void check(float*output, size_t len, float*standard_output){
    for(size_t i = 0;i < len; i++){
        REQUIRE(output[i] == Approx(standard_output[i]).margin(1e-4));
    }
}

TEST_CASE( "softmax" ){
    const size_t len = 4;
    float output[len] = {0};

    SECTION("normal") {
        float data[]            = {0.4560,  0.1230,  0.6832, -1.3948 };
        float standard_output[] = {0.3196,  0.2291,  0.4011,  0.0502 };

        softmax(data, len, output);
        check(output, len, standard_output);

    }

    SECTION("extreme") {
        float data[]            = {110.4560, 149.123, 14.6832, -15.3948};
        float standard_output[] = { 0.0000,  1.0000,  0.0000,  0.0000};

        softmax(data, len, output);
        check(output, len, standard_output);
    }

}