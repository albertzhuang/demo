#include <cmath>
#include <algorithm>

#include "softmax.h"

void softmax(float *input, size_t len, float *output){
    float sum = 0.0f;

    float max_value = *std::max_element(input, input+len);

    for( size_t i = 0; i < len; i++){
        output[i] = exp(input[i] - max_value);
        sum += output[i];
    }
    for( size_t i = 0; i < len; i++){
        output[i] /= sum;
    }
}