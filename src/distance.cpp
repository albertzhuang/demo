#include "distance.h"
#include <cmath>
#include <algorithm>

void distance( float* input1, float* input2, size_t N, float* output ){
    float *diffs = new float[N];
    for( size_t i = 0; i < N; i++){
        diffs[i] = fabs(input1[i] - input2[i]);
    }
    float max_value = *std::max_element(diffs, diffs+N);
    float sum = 0.0;
    for( size_t i = 0; i < N; i++){
        float diff = diffs[i] / max_value;
        sum += diff * diff;
    }
    *output = max_value * sqrt(sum);
    delete [] diffs;
}