CXXFLAGS:=-std=c++14 -Iinclude -Isrc -Wall -pedantic -DVERSION=${VERSION}

all: bin/main

test: bin/unittest
	./bin/unittest

bin obj:
	mkdir -p $@

bin/unittest: obj/test.o \
	obj/test_softmax.o obj/softmax.o \
	obj/test_distance.o obj/distance.o \
	| bin
	$(CXX) -o $@ $^

obj/%.o: src/%.cpp | obj
	$(CXX) $(CXXFLAGS) -c -o $@ $^

obj/test_%.o: src/tests/test_%.cpp | obj
	$(CXX) $(CXXFLAGS) -c -o $@ $^

clean:
	$(RM) *.o
	$(RM) -rf bin/ obj/


##########

bin/main: obj/main.o obj/softmax.o | bin
	$(CXX) -o $@ $^