FROM registry.gitlab.com/albertzhuang/demo/base:master

RUN mkdir /app
WORKDIR /app

ARG VERSION
ENV VERSION ${VERSION}

COPY . /app
RUN make
RUN rm -rf src

CMD ["./bin/main"]